# Calculator project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


### Run backend json-server for database
```
npm run backend
```

