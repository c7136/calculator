import {createRouter, createWebHistory} from 'vue-router'

import Home from '../views/Home'
import Calcs from '../views/Calcs'
import Data from '../views/Data'

const routes = [
    {
        path: '/calcs',
        name: 'Calcs',
        component: Calcs
    },
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/data',
        name: 'Data',
        component: Data
    }
    
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router